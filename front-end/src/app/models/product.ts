export interface Product {

  _id: number;
  name: string;
  description: string;
  price: number;
  count: number;
  photo: string;
  category: string;
  discount: {
    percent: number,
    duration: number,
    finishDate: Date
  }
}
