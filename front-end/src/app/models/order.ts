import {Product} from "./product";

export interface Order {

  _id: number;
  username: string;
  name: string;
  address: string;
  products: Array<Product>;
  completed: boolean;

}
