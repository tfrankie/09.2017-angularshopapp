import { Pipe, PipeTransform } from '@angular/core';
import {Product} from "../models/product";

@Pipe({
  name: 'category'
})
export class CategoryPipe implements PipeTransform {

  transform(products: Product[], selectedCategories: any, filteredCount: any): any {
    if(!products) return null;
    let filtered =  products.filter(product => {
      return selectedCategories.includes(product.category)
    });
    filteredCount.count = filtered.length;
    return filtered;
  }

}
