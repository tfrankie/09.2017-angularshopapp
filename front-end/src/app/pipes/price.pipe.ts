import { Pipe, PipeTransform } from '@angular/core';
import {Product} from "../models/product";

@Pipe({
  name: 'price'
})
export class PricePipe implements PipeTransform {

  transform(products: Product[], filteringPrice: any, filteredCount: any): any {
    if(!products) return null;
    let filtered = products.filter(product => {
      return product.price >= filteringPrice[0] && product.price <= filteringPrice[1];
    });
    filteredCount.count = filtered.length;
    return filtered;
  }

}
