import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'productDescription'
})
export class ProductDescriptionPipe implements PipeTransform {

  transform(productDescription: string): any {
    if(productDescription.length > 120){
      return productDescription.substring(0, 120) + " ...";
    }
    return productDescription;
  }

}
