import { Component, OnInit } from '@angular/core';
import {BsModalRef} from "ngx-bootstrap";
import {Product} from "../../../models/product";

@Component({
  selector: 'app-discount-modal',
  templateUrl: './discount-modal.component.html',
  styleUrls: ['./discount-modal.component.css']
})
export class DiscountModalComponent implements OnInit {

  product: Product;

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
  }

}
