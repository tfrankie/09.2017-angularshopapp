import {Component, OnInit} from "@angular/core";
import {CardService} from "../../../services/common/card.service";

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  constructor(private cardService: CardService) {
  }

  ngOnInit() {
  }

  removeFromCard(productIndex: number): void {
    this.cardService.removeFromCard(productIndex);
  }

}
