import {Component, OnInit} from "@angular/core";
import {BsModalRef} from "ngx-bootstrap";
import {Product} from "../../../models/product";

@Component({
  selector: 'app-product-details-modal',
  templateUrl: './product-details-modal.component.html',
  styleUrls: ['./product-details-modal.component.css']
})
export class ProductDetailsModalComponent implements OnInit {

  product: Product;

  constructor(public bsModalRef: BsModalRef) {
  }

  ngOnInit() {
  }

}
