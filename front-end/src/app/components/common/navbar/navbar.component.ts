import {Component, OnInit} from "@angular/core";
import {CardService} from "../../../services/common/card.service";
import {AuthService} from "../../../services/admin/auth.service";

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private cardService: CardService, private authService: AuthService) {
  }

  ngOnInit() {
  }


}
