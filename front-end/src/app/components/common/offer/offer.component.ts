import {Component, OnInit} from "@angular/core";
import {OfferService} from "../../../services/common/offer.service";
import {CardService} from "../../../services/common/card.service";
import {Product} from "../../../models/product";
import {Category} from "../../../models/category";
import {BsModalRef, BsModalService} from "ngx-bootstrap";
import {ProductDetailsModalComponent} from "../product-details-modal/product-details-modal.component";
import * as io from "socket.io-client";
import {DiscountModalComponent} from "../discount-modal/discount-modal.component";
import {AlertService} from "../../../services/common/alert.service";

@Component({
  selector: 'offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css'],
  providers: [OfferService]
})
export class OfferComponent implements OnInit {

  offer: Array<Product>;
  allCategories: Array<Category>;
  selectedCategories: Array<string>;
  loading = false;
  filteringText = "";
  filteringPriceRange = [0, 99999];
  filteredPrice = [0, 99999];
  filteredCount = {count: 0};
  page = 1;
  limit = 3;
  productDetailsModalRef: BsModalRef;
  discountModal: BsModalRef;

  constructor(private offerService: OfferService,
              private cardService: CardService,
              private modalService: BsModalService,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.offerService.getOffer().subscribe(offer => {
      this.offer = offer;
      this.allCategories = this.selectDistinctCategories(offer);
      this.selectedCategories = this.allCategories.map(category => category.name);
      this.initPriceFilters(offer);
    });

    this.setupSockets();
  }

  private setupSockets(){
    let socket = io.connect('http://localhost:3000');
    socket.on('discount' , discounted_product => {
      this.discountModal = this.modalService.show(DiscountModalComponent);
      this.discountModal.content.product = discounted_product;

      this.offer.forEach( (product, index) => {
        if (product._id == discounted_product._id){
          this.offer[index].discount = discounted_product.discount;
        }
      });
    });
    socket.on('discount-finished' , discounted_finished_product => {
      this.alertService.error("Koniec promocji produktu " + discounted_finished_product.name);

      this.offer.forEach( (product, index) => {
        if (product._id == discounted_finished_product._id){
          this.offer[index].discount = undefined;
        }
      });
    })
  }

  private selectDistinctCategories(offer: Product[]): Category[] {
    return offer.map((product) => {
      return product.category;
    })
      .filter((value, index, self) => {
        return self.indexOf(value) === index;
      })
      .map(obj => {
        return {
          name: obj,
          selected: false
        };
      });
  }

  addToCard(product: Product): void {
    this.cardService.addToCard(product);
  }

  private initPriceFilters(offer) {
    let maxProductPrice = Math.ceil(Math.max(...offer.map(prod => prod.price)) / 100) * 100;
    this.filteringPriceRange = [0, maxProductPrice];
    this.filteredPrice = [0, maxProductPrice];
  }

  onPriceFilterChange(event) {
    this.page = 1;
  }

  clearFilters(): void {
    this.filteringText = "";
    for (let category of this.allCategories) {
      category.selected = false;
    }
    this.initPriceFilters(this.offer);
    this.selectedCategories = this.allCategories.map(category => category.name)
    this.page = 1;
  }

  selectCategory() {
    this.selectedCategories = this.allCategories
      .filter(category => category.selected)
      .map(category => category.name)
    if (this.selectedCategories.length == 0) {
      this.selectedCategories = this.allCategories.map(category => category.name)
    }
    this.page = 1;
  }

  showDetails(product) {
    this.productDetailsModalRef = this.modalService.show(ProductDetailsModalComponent);
    this.productDetailsModalRef.content.product = product;
  }

  prevPage(event: boolean): void {
    this.page--;
  }

  nextPage(): void {
    this.page++;
  }

  goToPage(pageNr: number): void {
    this.page = pageNr;
  }

}
