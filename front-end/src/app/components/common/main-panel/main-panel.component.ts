import {Component, OnInit} from "@angular/core";
import {CardService} from "../../../services/common/card.service";

@Component({
  selector: 'app-main-panel',
  templateUrl: './main-panel.component.html',
  styleUrls: ['./main-panel.component.css']
})
export class MainPanelComponent implements OnInit {

  constructor(private cardService: CardService) {
  }

  ngOnInit() {
    this.cardService.fetchCard().subscribe(card => {});
  }

}
