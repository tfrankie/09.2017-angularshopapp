import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {Product} from "../../../models/product";

@Component({
  selector: 'product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  @Input() product: Product;
  @Output() addToCardEmitter = new EventEmitter<Product>();
  @Output() showDetailsEmitter = new EventEmitter<Product>();

  constructor() {
  }

  ngOnInit() {
  }

  addToCard(): void {
    this.addToCardEmitter.emit(this.product);
  }

  showDetails() {
    this.showDetailsEmitter.emit(this.product);
  }

}
