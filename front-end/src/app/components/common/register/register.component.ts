import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {AuthService} from "../../../services/admin/auth.service";
import {AlertService} from "../../../services/common/alert.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  model: any = {};
  loading = false;

  constructor(private router: Router,
              private authService: AuthService,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }

  register() {
    this.loading = true;
    this.authService.register(this.model.username, this.model.password)
      .subscribe(
        data => {
          this.alertService.success('Zarejestrowano pomyślnie.', true);
          this.router.navigate(['/login']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
