import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {Category} from "../../../models/category";

@Component({
  selector: 'category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  @Input() category: Category;
  @Output() categorySelected = new EventEmitter<Category>();

  constructor() {
  }

  ngOnInit() {
  }

  selectFilters(category: Category): void {
    category.selected = !category.selected;
    this.categorySelected.emit(category);
  }

}
