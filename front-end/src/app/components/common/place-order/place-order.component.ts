import {Component, OnInit} from "@angular/core";
import {CardService} from "../../../services/common/card.service";
import {OrderService} from "../../../services/common/order.service";
import {AlertService} from "../../../services/common/alert.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.css'],
  providers: [OrderService]
})
export class PlaceOrderComponent implements OnInit {

  name = ""
  address = ""
  invalidName: boolean;
  invalidAddress: boolean;

  constructor(private router: Router,
              private cardService: CardService,
              private orderService: OrderService,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }

  order(): void {
    this.onChangeName(this.name);
    this.onChangeAddress(this.address);
    if (!this.invalidName && !this.invalidAddress) {
      let order = {
        name: this.name,
        address: this.address,
        completed: false,
        products: this.cardService.card.map(product => {
          return {
            _id: product._id,
            name: product.name,
            description: product.description,
            price: product.price,
            photo: product.photo,
            count: product.count,
            category: product.category
          }
        })
      };
      this.orderService.addOrder(order).subscribe(createdOrder => {
        this.alertService.success('Dodano zamówienie.', true);
        this.router.navigate(['/']);
      })

      this.cardService.clearCard();
    }
  }

  onChangeName(newValue): void {
    if (newValue.length == 0) {
      this.invalidName = true;
    } else {
      this.invalidName = false;
    }
  }

  onChangeAddress(newValue): void {
    if (newValue.length == 0) {
      this.invalidAddress = true;
    } else {
      this.invalidAddress = false;
    }
  }
}
