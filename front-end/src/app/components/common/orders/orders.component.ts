import {Component, OnInit} from "@angular/core";
import {Order} from "../../../models/order";
import {OrderService} from "../../../services/common/order.service";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
  providers: [OrderService]
})
export class OrdersComponent implements OnInit {

  orders: Array<Order>;

  constructor(private orderService: OrderService) {
  }

  ngOnInit() {
    this.orderService.getUserOrders().subscribe(orders => {
      this.orders = orders;
    });
  }

}
