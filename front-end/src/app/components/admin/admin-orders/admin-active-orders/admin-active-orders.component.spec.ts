import {async, ComponentFixture, TestBed} from "@angular/core/testing";

import {AdminActiveOrdersComponent} from "./admin-active-orders.component";

describe('AdminActiveOrdersComponent', () => {
  let component: AdminActiveOrdersComponent;
  let fixture: ComponentFixture<AdminActiveOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdminActiveOrdersComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminActiveOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
