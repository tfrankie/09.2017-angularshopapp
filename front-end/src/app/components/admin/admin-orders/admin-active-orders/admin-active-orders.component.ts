import {Component, OnInit} from "@angular/core";
import {OrderService} from "../../../../services/common/order.service";
import {Order} from "../../../../models/order";

@Component({
  selector: 'app-admin-active-orders',
  templateUrl: './admin-active-orders.component.html',
  styleUrls: ['./admin-active-orders.component.css'],
  providers: [OrderService]
})
export class AdminActiveOrdersComponent implements OnInit {

  orders: Array<Order>;

  constructor(private orderService: OrderService) {
  }

  ngOnInit() {
    this.orderService.getActiveOrders().subscribe(orders => {
      this.orders = orders;
    });
  }

  realizeOrder(order: Order) {
    this.orders.splice(this.orders.indexOf(order), 1);

    order.completed = true;
    this.orderService.saveOrder(order).subscribe(orders => {
      // this.orders = orders;
    });
  }

}
