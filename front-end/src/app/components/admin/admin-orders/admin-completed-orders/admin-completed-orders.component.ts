import {Component, OnInit} from "@angular/core";
import {OrderService} from "../../../../services/common/order.service";
import {Order} from "../../../../models/order";

@Component({
  selector: 'app-admin-completed-orders',
  templateUrl: './admin-completed-orders.component.html',
  styleUrls: ['./admin-completed-orders.component.css'],
  providers: [OrderService]
})
export class AdminCompletedOrdersComponent implements OnInit {

  orders: Array<Order>;

  constructor(private orderService: OrderService) {
  }

  ngOnInit() {
    this.orderService.getCompletedOrders().subscribe(orders => {
      this.orders = orders;
    });
  }

}
