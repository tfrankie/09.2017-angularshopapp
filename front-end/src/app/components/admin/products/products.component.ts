import {Component, OnInit} from "@angular/core";
import {Product} from "../../../models/product";
import {Category} from "../../../models/category";
import {OfferService} from "../../../services/common/offer.service";
import {AdminProductService} from "../../../services/admin/admin-product.service";
import {AlertService} from "../../../services/common/alert.service";
import {DiscountService} from "../../../services/admin/discount.service";
import * as io from "socket.io-client";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  providers: [OfferService, AdminProductService, DiscountService]
})
export class ProductsComponent implements OnInit {

  offer: Array<Product>;
  allCategories: Array<Category>;
  selectedCategories: Array<string>;
  loading = false;
  filteringText = "";
  filteringPriceRange = [0, 99999];
  filteredPrice = [0, 99999];
  filteredCount = {count: 0};
  page = 1;
  limit = 3;

  addingNew = false;
  newProductName: string;
  newProductCategory: string;
  newProductDescription: string;
  newProductPhoto: string;
  newProductPrice: number;

  constructor(private offerService: OfferService,
              private adminProductService: AdminProductService,
              private alertService: AlertService,
              private discountService: DiscountService) {
  }

  ngOnInit() {
    this.offerService.getOffer().subscribe(offer => {
      this.offer = offer;
      this.allCategories = this.selectDistinctCategories(offer);
      this.selectedCategories = this.allCategories.map(category => category.name);
      this.initPriceFilters(offer);
    });
    this.initAddingFields();
    this.setupSockets();
  }

  private setupSockets(){
    let socket = io.connect('http://localhost:3000');
    socket.on('discount-finished' , discounted_finished_product => {
      this.alertService.error("Koniec promocji produktu " + discounted_finished_product.name);

      this.offer.forEach( (product, index) => {
        if (product._id == discounted_finished_product._id){
          this.offer[index].discount = undefined;
        }
      });
    })
  }

  private selectDistinctCategories(offer: Product[]): Category[] {
    return offer.map((product) => {
      return product.category;
    })
      .filter((value, index, self) => {
        return self.indexOf(value) === index;
      })
      .map(obj => {
        return {
          name: obj,
          selected: false
        };
      });
  }

  saveEditing(product: Product): void {
    this.adminProductService.saveProduct(product)
      .subscribe(savedProduct => {
      });
    this.allCategories = this.selectDistinctCategories(this.offer);
    this.selectCategory();
  }

  deleteProduct(product: Product): void {
    let index = this.offer.indexOf(product);
    if (index === -1) {
      return;
    }
    this.offer.splice(index, 1);
    this.offer = this.offer.slice();
    this.allCategories = this.selectDistinctCategories(this.offer);
    this.selectCategory();

    this.adminProductService.deleteProduct(product)
      .subscribe(deletedId => {
      });
  }

  cancelAddingProduct(): void {
    this.initAddingFields();
    this.addingNew = false;
  }

  addProduct(): void {
    let newProduct = {
      name: this.newProductName,
      category: this.newProductCategory,
      description: this.newProductDescription,
      photo: this.newProductPhoto,
      price: this.newProductPrice
    };
    this.addingNew = false;

    this.adminProductService.addProduct(newProduct)
      .subscribe(createdProduct => {
        this.offer.push(createdProduct);
        this.allCategories = this.selectDistinctCategories(this.offer);
        this.selectCategory();
      });
  }

  initAddingFields(): void {
    this.newProductName = "";
    this.newProductCategory = "";
    this.newProductDescription = "";
    this.newProductPhoto = "";
    this.newProductPrice = 0;
  }


  clearFilters(): void {
    this.filteringText = "";
    for (let category of this.allCategories) {
      category.selected = false;
    }
    this.initPriceFilters(this.offer);
    this.selectedCategories = this.allCategories.map(category => category.name)
    this.page = 1;
  }

  private initPriceFilters(offer) {
    let maxProductPrice = Math.ceil(Math.max(...offer.map(prod => prod.price)) / 100) * 100;
    this.filteringPriceRange = [0, maxProductPrice];
    this.filteredPrice = [0, maxProductPrice];
  }

  onPriceFilterChange(event) {
    this.page = 1;
  }

  selectCategory() {
    this.selectedCategories = this.allCategories
      .filter(category => category.selected)
      .map(category => category.name)
    if (this.selectedCategories.length == 0) {
      this.selectedCategories = this.allCategories.map(category => category.name)
    }
    this.page = 1;
  }

  addDiscount(discount){
    this.discountService.addDiscount(discount).subscribe(discounted_product => {
      this.offer.forEach( (product, index) => {
        if (product._id == discounted_product._id){
          this.offer[index].discount = discounted_product.discount;
        }
      });
      this.alertService.success("Dodano promocję.")
    });
  }

  prevPage(event: boolean): void {
    this.page--;
  }

  nextPage(): void {
    this.page++;
  }

  goToPage(pageNr: number): void {
    this.page = pageNr;
  }

}
