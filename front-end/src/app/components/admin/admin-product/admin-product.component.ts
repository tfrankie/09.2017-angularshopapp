import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {Product} from "../../../models/product";

@Component({
  selector: 'admin-product',
  templateUrl: './admin-product.component.html',
  styleUrls: ['./admin-product.component.css']
})
export class AdminProductComponent implements OnInit {

  @Input() product: Product;
  @Output() saveProduct = new EventEmitter<Product>();
  @Output() deleteProduct = new EventEmitter<Product>();
  @Output() addDiscount = new EventEmitter<any>();

  editing = false;
  editingName: string;
  editingCategory: string;
  editingDescription: string;
  editingPhoto: string;
  editingPrice: number;
  editingCount: number;

  settingDiscount = false;
  discountPercent = 0;
  discountDuration = 0;

  constructor() {
  }

  ngOnInit() {
    this.initEditFields();
  }

  initEditFields(): void {
    this.editingName = this.product.name;
    this.editingCategory = this.product.category;
    this.editingDescription = this.product.description;
    this.editingPhoto = this.product.photo;
    this.editingPrice = this.product.price;
    this.editingCount = this.product.count;
  }

  cancelEditing(): void {
    this.initEditFields();
    this.editing = false;
  }

  saveEditing(): void {
    this.product.name = this.editingName;
    this.product.category = this.editingCategory;
    this.product.description = this.editingDescription;
    this.product.photo = this.editingPhoto;
    this.product.price = this.editingPrice;
    this.product.count = this.editingCount;
    this.editing = false;
    this.saveProduct.emit(this.product);
  }

  delete(): void {
    if (confirm("Usunać produkt?")) {
      this.deleteProduct.emit(this.product);
    }
  }

  cancelSettingDiscount(){
    this.discountPercent = 0;
    this.discountDuration = 0;
    this.settingDiscount = false;
  }

  saveDiscount(){
    let discount = {
      product: this.product,
      percent: this.discountPercent,
      duration: this.discountDuration,
      finishDate: new Date(new Date().getTime() + this.discountDuration*60000)
    };
    this.addDiscount.emit(discount);
    this.cancelSettingDiscount();
  }

}
