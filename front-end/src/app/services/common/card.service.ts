import {Injectable} from "@angular/core";
import {Product} from "../../models/product";
import {AuthService} from "../admin/auth.service";
import {Headers, Http, RequestOptions} from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import {AlertService} from "./alert.service";

@Injectable()
export class CardService {

  private baseUrl = 'http://localhost:3000';
  public card = new Array<Product>();
  cardValue = 0;
  cardCount = 0;

  constructor(private http: Http, private authService: AuthService, private alertService: AlertService) {
  }

  fetchCard() {
    let headers = new Headers({'x-access-token': this.authService.token});
    let options = new RequestOptions({headers: headers});

    let username = this.authService.getLoggedUsername();

    return this.http.get(this.baseUrl + '/api/user/' + username + '/card', options)
      .map(res => {
        this.card = res.json();
        this.cardCount = this.card.reduce((total: number, current: Product) => {
          return total + current.count;
        }, 0);
        this.cardValue = this.card.reduce((total: number, current: Product) => {
          return total + current.price;
        }, 0);
        return res.json()
      });
  }

  addToCard(product: Product): void {
    let offerProduct = this.getProductFromOfferById(product._id);
    if (offerProduct) {
      if (offerProduct.count == product.count) {
        this.alertService.error("Brak dostępnych produktów");
        return;
      }
      if(product.discount){
        offerProduct.price += product.price * ((100 - product.discount.percent) / 100)
      } else {
        offerProduct.price += product.price;
      }
      offerProduct.count++;
    } else {
      let newProduct = {
        count: 1,
        _id: product._id,
        name: product.name,
        description: product.description,
        photo: product.photo,
        price: product.price,
        category: product.category,
        discount: undefined
      };
      if(product.discount){
        newProduct.price = product.price * ((100 - product.discount.percent) / 100)
      }
      this.card.push(newProduct);
    }
    this.cardCount++;
    if(product.discount){
      this.cardValue += product.price * ((100 - product.discount.percent) / 100)
    } else {
      this.cardValue += product.price;
    }

    this.saveCard().subscribe(card => {
    });
  }

  getProductFromOfferById(id) {
    return this.card.find(product => {
      return product._id == id;
    })
  }

  saveCard() {
    let headers = new Headers({'x-access-token': this.authService.token});
    let options = new RequestOptions({headers: headers});
    let username = this.authService.getLoggedUsername();

    return this.http.put(this.baseUrl + '/api/user/' + username + '/card', this.card, options)
      .map(res => res.json());
  }

  removeFromCard(productIndex: number): void {
    let deletedProduct = this.card[productIndex];

    this.cardCount--;
    this.cardValue -= deletedProduct.price / deletedProduct.count;

    if (deletedProduct.count > 1) {
      deletedProduct.price -= deletedProduct.price / deletedProduct.count;
      deletedProduct.count--;
    } else {
      this.card.splice(productIndex, 1);
    }

    this.saveCard().subscribe(card => {
    });
  }


  clearCard() {
    this.card = [];
    this.cardValue = 0;
    this.cardCount = 0;

    // this.saveCard().subscribe(card => {});
  }

}
