import { Injectable } from '@angular/core';
import { Product } from '../../models/product'
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import 'rxjs/add/operator/map'

@Injectable()
export class OfferService {

  private baseUrl = 'http://localhost:3000';

  constructor(private http: Http) {}

  getOffer() : Observable<Product[]>{
    return this.http.get(this.baseUrl + '/product')
      .map(res => res.json());
  }

}
