import { Injectable } from '@angular/core';
import {Order} from "../../models/order";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs";
import {Headers} from '@angular/http';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import {AuthService} from "../admin/auth.service";

@Injectable()
export class OrderService {

  private baseUrl = 'http://localhost:3000';

  constructor(private http: Http, private authService: AuthService) {}

  addOrder(order): Observable<Order>{
    let request = {
      username: this.authService.getLoggedUsername(),
      order: order
    };
    let headers: Headers = new Headers({ 'Content-Type': 'application/json' });
    let options: RequestOptions = new RequestOptions({ headers: headers });
    return this.http.post(this.baseUrl + '/order', request, options)
      .map(res => res.json());
  }

  getActiveOrders() : Observable<Order[]>{
    let headers = new Headers({ 'x-access-token': this.authService.token });
    let options = new RequestOptions({ headers: headers, });
    return this.http.get(this.baseUrl + '/api/order?completed=false', options)
      .map(res => res.json());
  }

  getCompletedOrders() : Observable<Order[]>{
    let headers = new Headers({ 'x-access-token': this.authService.token });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.baseUrl + '/api/order?completed=true', options)
      .map(res => res.json());
  }

  getUserOrders() : Observable<Order[]>{
    let headers = new Headers({ 'x-access-token': this.authService.token });
    let options = new RequestOptions({ headers: headers });

    let username = this.authService.getLoggedUsername();

    return this.http.get(this.baseUrl + '/api/user/' + username + '/order', options)
      .map(res => res.json());
  }

  saveOrder(order: Order) : Observable<Order[]> {
    let headers = new Headers({ 'x-access-token': this.authService.token });
    let options = new RequestOptions({ headers: headers });

    return this.http.put(this.baseUrl + '/api/user/' + order.username + '/order', order, options)
      .map(res => res.json());
  }
}
