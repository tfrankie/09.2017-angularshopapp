import { Injectable } from '@angular/core';
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs";
import {Headers} from '@angular/http';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import {AlertService} from "../common/alert.service";
import {Router} from "@angular/router";

@Injectable()
export class AuthService {

  private baseUrl = 'http://localhost:3000';
  headers: Headers = new Headers({ 'Content-Type': 'application/json' });
  options: RequestOptions = new RequestOptions({ headers: this.headers });
  public token: string;

  constructor(private http: Http, private router: Router, private alertService: AlertService) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  login(username: string, password: string): Observable<boolean> {
    let user = {
      username: username,
      password: password
    };
    return this.http.post(this.baseUrl + '/authenticate', user, this.options)
      .map(response => {
        let jsonResponse = response.json();
        this.token = jsonResponse.token;
        if (jsonResponse && jsonResponse.token) {
          localStorage.setItem('currentUser', JSON.stringify(jsonResponse));
        }
        return jsonResponse;
      });
  }

  logout(): void {
    this.token = null;
    if (localStorage.getItem('currentUser')) {
      this.alertService.success('Wylogowano użytkownika.', true);
      this.router.navigate(['/']);
    }
    localStorage.removeItem('currentUser');
  }


  userLoggedIn(){
    if (localStorage.getItem('currentUser')) {
      return true;
    } else {
      return false;
    }
  }

  adminLoggedIn(){
    let user = JSON.parse(localStorage.getItem('currentUser'));
    if (!user) return false;
    return user.admin == true;
  }

  getLoggedUsername(){
    return JSON.parse(localStorage.getItem('currentUser')).username;
  }

  register(username: string, password: string) {
    let user = {
      username: username,
      password: password
    };
    return this.http.post(this.baseUrl + '/register', user, this.options)
      .map(response => {

        let user = response.json();
        // if (user && user.token) {
        //   localStorage.setItem('currentUser', JSON.stringify(user));
        // }
        return user;
      });
  }
}
