import { Injectable } from '@angular/core';
import {Product} from "../../models/product";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs";
import {Headers} from '@angular/http';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import {AuthService} from "./auth.service";

@Injectable()
export class AdminProductService {

  private baseUrl = 'http://localhost:3000';
  constructor(private http: Http, private authService: AuthService) { }

  saveProduct(product: Product): Observable<Product> {
    let headers = new Headers({
      'Content-Type': 'application/json',
      'x-access-token': this.authService.token
    });
    let options = new RequestOptions({ headers: headers });
    return this.http.put(this.baseUrl + '/api/product', product, options)
      .map(res => res.json());
  }

  deleteProduct(product: Product): Observable<string> {
    let headers = new Headers({
      'Content-Type': 'application/json',
      'x-access-token': this.authService.token
    });
    let options = new RequestOptions({ headers: headers });
    return this.http.delete(this.baseUrl + '/api/product/' + product._id, options)
      .map(res => res.json());
  }

  addProduct(newProduct): Observable<Product> {
    let headers = new Headers({
      'Content-Type': 'application/json',
      'x-access-token': this.authService.token
    });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.baseUrl + '/api/product', newProduct, options)
      .map(res => res.json());
  }
}
