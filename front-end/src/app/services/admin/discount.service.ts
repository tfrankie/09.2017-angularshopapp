import { Injectable } from '@angular/core';
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs";
import {Headers} from '@angular/http';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import {AuthService} from "./auth.service";

@Injectable()
export class DiscountService {

  private baseUrl = 'http://localhost:3000';

  constructor(private http: Http,
              private authService: AuthService) { }

  addDiscount(discount): Observable<any> {
    let headers = new Headers({
      'Content-Type': 'application/json',
      'x-access-token': this.authService.token
    });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.baseUrl + '/api/discount', discount, options)
      .map(res => res.json());
  }

}
