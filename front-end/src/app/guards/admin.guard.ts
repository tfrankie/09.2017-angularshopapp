import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {AuthService} from "../services/admin/auth.service";

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(private router: Router, private authService: AuthService){}

  canActivate( next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authService.adminLoggedIn()) {
      return true;
    } else if(this.authService.userLoggedIn()) {
      this.router.navigate(['/']);
      return false;
    }
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    return false;
  }

}
