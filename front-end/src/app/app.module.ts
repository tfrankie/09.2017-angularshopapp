import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './components/app/app.component';
import { NavbarComponent } from './components/common/navbar/navbar.component';
import { OfferComponent } from './components/common/offer/offer.component';
import { ProductComponent } from './components/common/product/product.component';
import { PaginationComponent } from './components/common/pagination/pagination.component';
import { CategoryComponent } from './components/common/category/category.component';
import { CardComponent } from './components/common/card/card.component';

import { CardService } from './services/common/card.service';
import { PlaceOrderComponent } from './components/common/place-order/place-order.component';
import { MainPanelComponent } from './components/common/main-panel/main-panel.component';
import { AdminPanelComponent } from './components/admin/admin-panel/admin-panel.component';
import { AdminOrdersComponent } from './components/admin/admin-orders/admin-orders.component';
import { ProductsComponent } from './components/admin/products/products.component';
import { AdminProductComponent } from './components/admin/admin-product/admin-product.component';
import { CategoryPipe } from './pipes/category.pipe';
import { PaginationPipe } from './pipes/pagination.pipe';
import { NamePipe } from './pipes/name.pipe';
import { PricePipe } from './pipes/price.pipe';
import { NouisliderModule } from 'ng2-nouislider/src/nouislider';
import {AdminGuard} from "./guards/admin.guard";
import { LoginComponent } from './components/common/login/login.component';
import {AuthService} from "./services/admin/auth.service";
import {BsDropdownModule, ModalModule} from "ngx-bootstrap";
import { ProductDetailsModalComponent } from './components/common/product-details-modal/product-details-modal.component';
import { ProductDescriptionPipe } from './pipes/product-description.pipe';
import { RegisterComponent } from './components/common/register/register.component';
import { AlertComponent } from './components/common/alert/alert.component';
import {AlertService} from "./services/common/alert.service";
import {UserLoggedGuard} from "./guards/user-logged.guard";
import {OrdersComponent} from "./components/common/orders/orders.component";
import { AdminActiveOrdersComponent } from './components/admin/admin-orders/admin-active-orders/admin-active-orders.component';
import { AdminCompletedOrdersComponent } from './components/admin/admin-orders/admin-completed-orders/admin-completed-orders.component';
import { DiscountModalComponent } from './components/common/discount-modal/discount-modal.component';


export const ROUTES: Routes = [
    { path: '', component: MainPanelComponent,
      children: [
        { path: '', component: OfferComponent },
        { path: 'card', component: CardComponent },
        { path: 'orders', component: OrdersComponent, canActivate: [UserLoggedGuard] },
        { path: 'order', component: PlaceOrderComponent, canActivate: [UserLoggedGuard] }
      ]
    },
    { path: 'admin', component: AdminPanelComponent, canActivate: [AdminGuard],
      children: [
        { path: '', redirectTo: 'products', pathMatch: 'full' },
        { path: 'products', component: ProductsComponent },
        { path: 'orders', component: AdminOrdersComponent, children: [
          { path: '', redirectTo: 'active', pathMatch: 'full' },
          { path: 'active', component: AdminActiveOrdersComponent },
          { path: 'completed', component: AdminCompletedOrdersComponent }
        ] }
      ]
    },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: '**', redirectTo: '' }
];
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    OfferComponent,
    ProductComponent,
    PaginationComponent,
    CategoryComponent,
    CardComponent,
    PlaceOrderComponent,
    MainPanelComponent,
    AdminPanelComponent,
    AdminOrdersComponent,
    ProductsComponent,
    AdminProductComponent,
    CategoryPipe,
    PaginationPipe,
    NamePipe,
    PricePipe,
    LoginComponent,
    ProductDetailsModalComponent,
    ProductDescriptionPipe,
    RegisterComponent,
    AlertComponent,
    OrdersComponent,
    AdminActiveOrdersComponent,
    AdminCompletedOrdersComponent,
    DiscountModalComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES),
    FormsModule,
    HttpModule,
    NouisliderModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot()
  ],
  entryComponents: [
    ProductDetailsModalComponent,
    DiscountModalComponent
  ],
  providers: [
    AlertService,
    CardService,
    AuthService,
    AdminGuard,
    UserLoggedGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
