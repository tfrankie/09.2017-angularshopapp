require( 'babel-core/register' );
const express = require('express');
var mongoose = require("mongoose");
var bodyParser = require('body-parser');
var config = require('./config');

const app = express();
var server = require('http' ).createServer(app);
var io = require('socket.io' )(server);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

mongoose.Promise = global.Promise;
mongoose.connect(config.database, {
    useMongoClient: true
});

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token");
    res.setHeader('Access-Control-Allow-Credentials', true);

    if ('OPTIONS' === req.method) {
        res.sendStatus(204);
    } else {
        next();
    }
});

require('./rest/token_auth')(app, express);
require('./rest/product_rest')(app);
require('./rest/order_rest')(app);
require('./rest/card_rest')(app);
app.set('superSecret', config.secret);
require('./rest/auth_rest')(app);

require('./rest/dicount_rest')(app, io);

// app.listen(3000, () => console.log('ShopApp back-end listening on port 3000!'));
server.listen(3000, () => console.log('ShopApp back-end listening on port 3000!'));