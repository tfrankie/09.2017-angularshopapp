const mongoose = require("mongoose");

const User = mongoose.model("User", new mongoose.Schema({
    id: Number,
    username: String,
    password: String,
    admin: Boolean,
    orders: [{
        id: Number,
        name: String,
        address: String,
        completed: Boolean,
        products: [{
            id: Number,
            name: String,
            description: String,
            price: Number,
            count: Number,
            photo: String,
            category: String,
            discount: {
                percent: Number,
                duration: Number,
                finishDate: Date
            }
        }]
    }],
    card: [{
        id: Number,
        name: String,
        description: String,
        price: Number,
        count: Number,
        photo: String,
        category: String,
        discount: {
            percent: Number,
            duration: Number,
            finishDate: Date
        }
    }]
}));

module.exports = {
    User
};