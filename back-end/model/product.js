const mongoose = require("mongoose");

const Product = mongoose.model("Product", new mongoose.Schema({
    id: Number,
    name: String,
    description: String,
    price: Number,
    count: Number,
    photo: String,
    category: String,
    discount: {
        percent: Number,
        duration: Number,
        finishDate: Date
    }
}));

module.exports = {
    Product,
};