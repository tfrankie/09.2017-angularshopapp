const model = require('../model/user.js');
const jwt = require('jsonwebtoken');

module.exports = function (app) {

    app.post("/authenticate", (req, res) => {
        let query = {'username': req.body.username};
        model.User.findOne(query, function (err, user) {
            if (err) throw err;
            if (!user) {
                res.json({success: false, message: 'Authentication failed. User not found.'});
            } else {
                if (user.password !== req.body.password) {
                    res.json({success: false, message: 'Authentication failed. Wrong password.'});
                } else {
                    const payload = {user: user.username};
                    let token = jwt.sign(payload, app.get('superSecret'), {expiresIn: 86400});
                    res.json({
                        token: token,
                        username: user.username,
                        admin: user.admin
                    });
                }
            }
        });
    });

    app.post("/register", (req, res) => {
        let user = new model.User(req.body);
        user.admin = false;
        user.orders = [];
        user.card = [];
        user.save().then(item => {
            res.send(user);
        }).catch(err => {
            res.status(400).send(err);
        });
    });

};