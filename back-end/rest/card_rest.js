const model = require('../model/user.js');

module.exports = function (app) {

    app.get('/api/user/:username/card', (req, res) => {
        let query = {'username': req.params.username};
        model.User.findOne(query, function (err, user) {
            res.send(user.card);
        });
    });

    app.put("/api/user/:username/card", (req, res) => {
        let query = {'username': req.params.username};
        model.User.findOne(query, function (err, user) {
            user.card = req.body;
            user.save().then(item => {
                res.send(req.body);
            }).catch(err => {
                res.status(400).send(err);
            });
        });
    });

};