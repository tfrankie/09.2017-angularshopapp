const productModel = require('../model/product.js');

var sockets = []

module.exports = function (app, io) {

    io.on('connection' , function(socket) {
        sockets.push(socket);

        socket.on('disconnect', function() {
            sockets.splice(sockets.indexOf(socket), 1);
        });
    });

    app.post("/api/discount", (req, res) => {
        let query = {'_id': req.body.product._id};
        productModel.Product.findOne(query, function (err, product) {
            product.discount = {
                percent: req.body.percent,
                duration: req.body.duration,
                finishDate: req.body.finishDate,
            };
            product.save().then(item => {
                setTimeout(function(){
                    item.discount = undefined;
                    item.save();
                    sockets.forEach( socket => {
                        socket.emit('discount-finished' , item);
                    });
                }, req.body.duration * 60 * 1000);

                sockets.forEach( socket => {
                    socket.emit('discount' , product);
                });

                res.send(product);
            }).catch(err => {
                res.status(400).send(err);
            });
        });
    });

};
