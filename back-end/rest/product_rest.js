const model = require('../model/product.js');

module.exports = function (app) {

    app.get('/product', (req, res) => {
        model.Product.find({}, function (err, products) {
            res.send(products);
        });
    });

    app.post("/api/product", (req, res) => {
        let product = new model.Product(req.body);
        product.save().then(item => {
            res.send(product);
        }).catch(err => {
            res.status(400).send(err);
        });
    });

    app.put('/api/product', (req, res) => {
        let updatedProduct = new model.Product(req.body);
        let query = {'_id': updatedProduct._id};
        model.Product.findOneAndUpdate(query, updatedProduct, function (err, doc) {
            if (err) return res.send(500, {error: err});
            return res.send(updatedProduct);
        });
    });

    app.delete('/api/product/:productId', (req, res) => {
        let productId = req.params.productId;
        model.Product.remove({_id: productId}, function (err, doc) {
            if (err) return res.send(500, {error: err});
            return res.send({productId});
        });
    });

};