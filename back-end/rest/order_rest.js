const userModel = require('../model/user.js');
const productModel = require('../model/product.js');

module.exports = function (app) {

    app.get('/api/order', (req, res) => {
        let ordersState = (req.query.completed == 'true');

        userModel.User.find({}, function (err, users) {
            res.send([].concat.apply([], users.filter(user => {
                return user.orders.length > 0;
            }).map(user => {
                return user.orders
                    .filter(order => {
                        return order.completed == ordersState;
                    })
                    .map(order => {
                        return {
                            username: user.username,
                            _id: order._id,
                            name: order.name,
                            address: order.address,
                            products: order.products,
                            completed: order.completed
                        };
                    });
            })));
        });
    });

    app.post("/order", (req, res) => {
        let query = {'username': req.body.username};
        userModel.User.findOne(query, function (err, user) {
            user.orders.push(req.body.order);
            user.card = [];

            req.body.order.products.forEach(product => {
                productModel.Product.findOne({'_id': product._id}, function (err, db_product) {
                    db_product.count -= product.count;
                    db_product.save();
                });
            })

            user.save().then(item => {
                res.send(req.body.order);
            }).catch(err => {
                res.status(400).send(err);
            });
        });
    });

    app.get('/api/user/:username/order', (req, res) => {
        let query = {'username': req.params.username};
        userModel.User.findOne(query, function (err, user) {
            res.send(user.orders);
        });
    });

    app.put('/api/user/:username/order', (req, res) => {
        let query = {'username': req.params.username};
        let order = {
            _id: req.body._id,
            name: req.body.name,
            address: req.body.address,
            products: req.body.products,
            completed: req.body.completed
        };
        userModel.User.findOne(query, function (err, user) {
            user.orders.forEach((userOrder, index) => {
                if (userOrder._id == order._id) {
                    user.orders[index] = order;
                }
            });
            user.save().then(item => {
                res.send(req.body);
            }).catch(err => {
                res.status(400).send(err);
            });
        });
    });

};
